import * as LISP from '../jli/lisp.js';
import { TURTLEOS } from '../../turtleos.js';


const COMMANDS = {
    ...LISP.LISP_COMMANDS,
    "pkgconf": (TOOLS, ...funcs) => {
        let res;
        funcs.forEach(f=>{
            res=TOOLS.run(f);
        });
        return res;
    },
    "set": (TOOLS, key, value) => {
        TOOLS.global[key]=value;
    },
    "get": (TOOLS, key) => {
        let r = TOOLS.global[key];
        if(!r) r="";
        return r;
    },
    "attempt-build": (TOOLS, bool) => {
        let envoir = TOOLS.global;
        const get = (key) => {
            let r = envoir[key];
            if(!r) TOOLS.throw(`${key} undefined`);
            return r;
        };
        let conf = {
            id: get('package.id'),
            name: get('package.name'),
            description: get('package.description'),
            version: get('package.version'),
            screenshots: get('package.screenshots'),
            permissions: get('package.permissions'),
            icon: get('package.icon'),
            categories: get('package.categories'),
            defaults: get('package.defaults'),
            developer: {
                profile: get('package.developer.profile'),
                nick: get('package.developer.nick')
            },
            type: get('package.type')
        };
        conf=JSON.stringify(conf);
        if(bool) TOOLS.print([
            "col",
            [conf, "blue"]
        ]);
        return conf;
    }
};
const BLACKLIST = [
    ...LISP.LISP_BLACKLIST,
    "pkgconf"
];
const STATE = {
    //...LISP.LISP_STATE,
};
Object.entries(TURTLEOS.release).forEach(([key, value])=>{
    STATE[`env.${key}`]=value;
});

export {
    COMMANDS,
    STATE,
    BLACKLIST
}
