import * as _ from 'lodash';
import * as JSZip from 'jszip';

import * as PKGLANG from './jli-lang-pkgconf';
import { JLI, JLI_DEFAULTS } from '../jli/jli';
import { pm } from '../../layers/turtleos-pm/pm';
import { PackageLayer } from '../../layers/turtleos-am/am';
import { json2react } from '../json2react/json2react';
import { PERMISSION } from '../turtleos-permissions/permissions';
import { FilePermission } from '../turtleos-rootfs/rootfs';
import { File, Folder } from '../virtfs/virtfs';

const config = {
    manifest: {
        supportedIconFormats: ["png"],
        supportedScreenshotFormat: ["png","jpg","jpeg"],
        categories: ["games", "office", "tools", "communication", "entertainment"],
        types: ['shell', 'ui']
    },
    source: {
        requiredFunctions: ['render']
    }
};

const checkAppManifest = ( manifest ) => {
    const todo = {
        "icon": (data) => {
            // check if icon is string
            if (typeof data !== "string") throw new Error('Icon has to be string');
            // if icon dataurl
            if(data.indexOf('data')===0)return true;
            // check if icon is url
            let url = new URL(data);
            // check if urls image name indicates a supported type
            if(config.manifest.supportedIconFormats.indexOf(url.pathname.split('.').pop()) === -1) throw new Error(`App Icon has to be one of types: ${config.manifest.supportedIconFormats}`);
            // all checks passed
            return true;
        },
        "id": (data) => {
            if (typeof data !== "string") throw new Error('AppID has to be string');
            if (data.match(new RegExp('\\.','g'))<3) throw new Error('ID has to contain at least three dots');
            if (data.indexOf('..')!==-1) throw new Error('One dot may not be followd by another one');
            return true;
        },
        "name": (data) => {
            if (typeof data !== "string") throw new Error('Appname has to be string');
            return true;
        },
        "description": (data) => {
            if (typeof data !== "string" && typeof data !== "object") throw new Error('Your description has to be either a string or a translateable object');
            return true;
        },
        "permissions": (data)=>{
            if (!Array.isArray(data)) throw new Error('App permissions have to be contained in an Array');
            data.forEach(perm=>{
                if(Object.values(PERMISSION).indexOf(perm)===-1) throw new Error('Unknown Permission detected');
            });
            return true;
        },
        "categories": (data) => {
            if (!Array.isArray(data)) throw new Error('App Category has to be Array');
            data.forEach(cat=>{
                if(!config.manifest.categories.includes(cat)) throw new Error(`Category ${cat} unknown`);
            });
            return true;
        },
        "screenshots": (data) => {
            if (!Array.isArray(data)) throw new Error('Screenshots must be contained in Array');
            data.forEach(img=>{
                let url = new URL(img);
                if( config.manifest.supportedScreenshotFormats.indexOf( url.pathname.split('.').pop() ) === -1) throw new Error(`App Screenshots have to be one of types: ${config.manifest.supportedScreenshotFormats}`);
            });
            return true;
        },
        "defaults": (data)=>{
            if(!Array.isArray(data)) throw new Error('App Defaults have to be contained in array');
            return true;
        },
        "developer": (data)=>{
            if(typeof data !== 'object') throw new Error('App Developer data has to be contained in an object');
            if(!data.profile) throw new Error('App Developer profile url missing');
            try {
                let profileURL = new URL(data.profile);
                if(!profileURL) throw new Error();
            } catch(e) {
                //profileurl not valid
                throw new Error('Profile URL invalid');
            }
            if(!data.nick || typeof data.nick !== 'string') throw new Error('Developer nickname missing');
        },
        version: (data) => {
            if(!Array.isArray(data)) throw new Error('Version has to be array');
            if(data.length<3) throw new Error('version should contain 3parts');
            return true;
        },
        type: (data) => {
            return (config.manifest.types.indexOf(data)===-1)?false:true;
        }
    };
    // validate each key
    Object.entries(todo).forEach(([key, value])=>{
        if (!manifest[key]) throw new Error(`Your App Manifest is missing a ${key} property`);
        value(manifest[key]);
    });
    return true;
};

// verifiy app response after initialisation
const verifySource = ( code, manifest ) => {
    let funcs = [
        ...config.source.requiredFunctions,
    ];
    funcs.forEach(func=>{
        if(typeof code[func] !== 'function') throw new Error(`Missing function ${func}`);
    });
    if(manifest.type==="shell") {
        if(typeof code.shell !== 'object') throw new Error('invalid Shell configuration');
        let opts = ["STATE", "BLACKLIST", "COMMANDS"];
        let cont = code.shell;
        opts.forEach(o=>{
            if(typeof cont[o] !== 'object') throw Error(`SHELL Object ${o} not Object`);
        });
    }
};

// prepare sourcecode for initialisation
const prepareSource = ( code ) => {
    // check if app attemts  registration
    if (code.indexOf('appdk.newApp')===-1) throw new Error('App not registered');

    // replace malicious code
    const replace_lookup_table = {
        ".parentNode": '',
        "appdk.newApp": 'return'
    };
    Object.entries(replace_lookup_table).forEach(([key, value])=>{
        code=code.replace(new RegExp(key,'g'), value);
    });
    return code;
};
const BuiltInOverwriteBlocks = ({
    appid='',
    appdk={}
}) => {
    let prefix = `${appid}.`;
    const fs = pm.getState().vfs;
    let d = fs.readFileSync(`/usr/apps/${appid}/ressources`);
    let R = {};
    if(!d.error) {
        try {
            R=JSON.parse(d.data.content);
        } catch(e) {
        }
    }
    return {
        appdk:appdk,
        "document": {
            getElementById: (id) => {
                return document.getElementById(prefix+id);
            },
            getElementsByClassName: (classname) => {
                return document.getElementByClassName(prefix+classname);
            },
            getElementsByName: (name) => {
                return document.gteElementByClassName(prefix+name);
            },
            createElement: (json) => {
                return json2react(json, prefix);
            }
        },
        R: R,
        "window": {
            
        },
        "localStorage": {
            setItem: (key, value) => {
                let file = fs.readFileSync(`/usr/apps/${appid}/shared_preferences`);
                if(file.error) return new Error(file);
                try {
                    let d = {...JSON.parse(file.data.content)};
                    d[key] = value;
                    let res = fs.writeFileSync(`/usr/apps/${appid}/shared_preferences`, JSON.stringify(d));
                    if(res.error) return new Error(res);
                    return true;
                } catch(e) {
                    return new Error(e);
                }
            },
            getItem: (key) => {
                let file = fs.readFileSync(`/usr/apps/${appid}/shared_preferences`);
                if(file.error) return new Error(file);
                try {
                    let d = JSON.parse(file.data.content);
                    return d[key];
                } catch(e) {
                    return new Error(e);
                }
            }
        },
        "setTimeout": setTimeout,
        "setInterval": setInterval,
        "fetch": (url, conf) => {
            // check for network permission
            const am = pm.getState().am;
            const nm = pm.getState().nm;
            if (!am.am.hasPermission(appid, PERMISSION.NETWORK)) return new Promise((res, rej) => rej({error:true, permission:false}));
            return nm.fetch(url, conf);
        }
    };
};
// overwrite javascript builtins
const overwriteBuiltIn = (output='keys',{
    blocks = BuiltInOverwriteBlocks,
}) => {
    return Object[output](blocks);
};
const initialiseApp = ( manifest, code, appdk ) => {
    try {
        checkAppManifest(manifest);
        // setup app
        let sourceReady = prepareSource(code);
        let sourceFunc = new Function(...overwriteBuiltIn('keys',{
            blocks: BuiltInOverwriteBlocks({
                appid: manifest.id,
                appdk: appdk
            })
        }), sourceReady);
        let source = sourceFunc(...overwriteBuiltIn('values',{
            blocks: BuiltInOverwriteBlocks({
                appid: manifest.id,
                appdk: appdk
            })
        }));
        verifySource(source, manifest);
        return {
            error: false,
            app: source
        };
    } catch (e) {
        return {
            error: true,
            msg: e
        };
    }
};



const generateManifest = ( txt ) => {
    let jli = JLI({
        COMMANDS: PKGLANG.COMMANDS,
        BLACKLIST: PKGLANG.BLACKLIST,
        STATE: PKGLANG.STATE,
        DEFAULTS: {
            ...JLI_DEFAULTS,
            error: (err)=>{throw new Error(err);},
            output: (msg)=>console.log(`JLI-PKGCONF: ERROR@${msg}`)
        },
    });
    return JSON.parse(jli.runLisp(txt));
};

const fetchApp = ( url ) => new Promise((resolve, reject) => {
    pm.getState().nm.fetch(url+'.toss')
        .then(b=>b.blob())
        .then(blob=>{
            let zip = new JSZip();
                        zip.loadAsync(blob)
                            .then(unzipped=>{
                                (async ()=> {
                                    try {
                                        let manifest = await zip.file('package.jli').async('string');
                                        let json = generateManifest(manifest);
                                        resolve({
                                            manifest: json,
                                            zip: zip
                                        });
                                    } catch(e) {
                                        reject(e);
                                    }
                                })();
                            });
                    })
                    .catch(e=>reject(e));
});

const uninstallApp = (id) => new Promise((res, rej)=>{
    let fold = pm.getState().vfs.readFolderSync('/usr/apps');
    if(fold.error) throw new Error('Error reading app dir');
    // remove old version completly
    if(Object.keys(fold.data.content).includes(id)) {
        if(pm.getState().vfs.deleteFolderSync(`/usr/apps/${id}`).error) rej('couldn\'t delete folder');
        res('App uninstalled');
    } else {
        rej('App not even installed');
    }
});

const extractContent = (zip) => new Promise((resolve, reject)=>{
    const R = {};
    let cont = [];
    let count = -1;
    let writer = R;
    zip.folder('ressources').forEach((re, file)=>cont.push([re, file]));
    const customloop = () => {
        count+=1;
        if(count>=cont.length) {
            resolve(R);
        } else {
            let [re, file] = cont[count];
        if(file.dir) {
            let d = re.slice(0, -1);
            if(!R[d]) R[d]={};
            writer=R[d];
            customloop();
        } else {
            let fn = re.split("/").pop().split('.')[0];
            zip.folder("ressources").file(re).async("base64").then(data=>{
                writer[fn]=data;
                customloop();
            })
                .catch(e=>{
                    reject({
                        e:e,
                        msg:'Couldnt save file'
                    });
            });
        }
        }
    };
    customloop();
});

const installApp = ( url ) => new Promise(async (resolve, reject)=>{
    try {
        fetchApp(url)
            .then(async({manifest, zip})=>{
                let js = await zip.file('src/main.js').async('string');
                let content = [];
                zip.forEach(d=>content.push(d));
                // Prepare installation
                let resp = initialiseApp(manifest, js.trim());
                if(resp.error) reject("Error initalising App: " + resp.msg);
                let fold = pm.getState().vfs.readFolderSync('/usr/apps');
                if(fold.error) reject('Error reading app dir');
                // remove old version completly
                if(Object.keys(fold.data.content).includes(manifest.id)) {
                    if(pm.getState().vfs.deleteFolderSync(`/usr/apps/${manifest.id}`).error) reject('couldn\'t delete folder');
                }
                let R = await extractContent(zip);
// create folder structure
        pm.getState().nm.fetch(manifest.icon)
            .then(b=>b.blob())
                    .then(blob=>{
                let reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = () => {
        let folder = pm.getState().vfs.makeFolderSync('/usr/apps',manifest.id, {
            permission: FilePermission.DEFAULT,
            content: {
                shared_preferences: new File({
                    permission: FilePermission.DEFAULT,
                    content: JSON.stringify({first_opened:true}),
                    bType: 'application/json'
                }),
                src: new Folder({
                    permission: FilePermission.DEFAULT,
                    content:{
                        manifest: new File({
                            permission: FilePermission.DEFAULT,
                            bType: 'application/json',
                            content: JSON.stringify(manifest)
                        }),
                        main: new File({
                            permission: FilePermission.DEFAULT,
                            bType: 'application/javascript',
                            content: js.trim()
                        }),
                        icon: new File({
                            permission: FilePermission.DEFAULT,
                            bType:'image/png',
                            content:reader.result
                        })
                    }
                }),
                ressources: new File({
                    permission: FilePermission.DEFAULT,
                    content: JSON.stringify(R),
                    bType:'application/json'
                }),
                permissions: new File({
                    permission: FilePermission.DEFAULT,
                    content:{},
                    bType:'application/json'
                })
            }
        });
                    if(folder.error) reject('Error creating app folder');
                    //installed
                    addAppToApps(
                        manifest,
                        js.trim(),
                        reader.result
                    );
                    resolve(true);
                };
            })
            .catch(e=>{
                reject(e);
            });
            })
            .catch(e=>reject(e));
    } catch(e) {
        reject(e);
    }
});


// old code used to get tos to build
const addAppToApps = (manifest, src, icon) => {
    pm.getState().am.am.addApp(new PackageLayer(
        manifest,
        src,
        icon
    ));
};

const listInstalledApps = () =>{
    try {
        let folder = pm.getState().vfs.readFolderSync('/usr/apps');
        if(folder.error) throw new Error('Could not read app folder');
        let list = Object.entries(folder.data.content).map(([id, folder])=>{
            
            return {
                manifest:JSON.parse(folder.content.src.content.manifest.content),
                src: folder.content.src.content.main.content,
                id:id,
                icon: folder.content.src.content.icon.content
            };
        });
        return {
            error: false,
            msg:null,
            data: list
        };
    } catch(e) {
        return {
            error: true,
            msg: e,
            data: []
        };
    }
};


export {
    generateManifest,
    //old shit
    addAppToApps,
    initialiseApp,
    listInstalledApps,
    fetchApp,
    installApp,
    uninstallApp
}
