import { json2react } from '../json2react/json2react';
import { PERMISSION } from '../turtleos-permissions/permissions';
import { pm } from '../../layers/turtleos-pm/pm';
import { FilePermission } from '../turtleos-rootfs/rootfs';
import { File, Folder } from '../virtfs/virtfs';
import { PackageLayer } from '../../layers/turtleos-am/am';

import * as _ from 'lodash';
import * as JSZip from 'jszip';

const config = {
    manifest: {
        supportedIconFormats: ["png"],
        supportedScreenshotFormat: ["png","jpg","jpeg"],
        categories: ["games", "office", "tools", "communication", "entertainment"],
    },
    source: {
        requiredFunctions: ['render', 'test', 'packageInfo']
    }
};

// check manifest for errors
const checkAppManifest = ( manifest ) => {
    const todo = {
        "icon": (data) => {
            // check if icon is string
            if (typeof data !== "string") throw new Error('Icon hast do be string');
            // check if icon is url
            let url = new URL(data);
            // check if urls image name indicates a supported type
            if(config.manifest.supportedIconFormats.indexOf(url.pathname.split('.').pop()) === -1) throw new Error(`App Icon has to be one of types: ${config.manifest.supportedIconFormats}`);
            // all checks passed
            return true;
        },
        "id": (data) => {
            if (typeof data !== "string") throw new Error('AppID has to be string');
            if (data.match(new RegExp('\\.','g'))<3) throw new Error('ID has to contain at least three dots');
            if (data.indexOf('..')!==-1) throw new Error('One dot may not be followd by another one');
            return true;
        },
        "name": (data) => {
            if (typeof data !== "string") throw new Error('Appname has to be string');
            return true;
        },
        "description": (data) => {
            if (typeof data !== "string" && typeof data !== "object") throw new Error('Your description has to be either a string or a translateable object');
            return true;
        },
        "permissions": (data)=>{
            if (!Array.isArray(data)) throw new Error('App permissions have to be contained in an Array');
            data.forEach(perm=>{
                if(Object.values(PERMISSION).indexOf(perm)===-1) throw new Error('Unknown Permission detected');
            });
            return true;
        },
        "categories": (data) => {
            if (!Array.isArray(data)) throw new Error('App Category has to be Array');
            data.forEach(cat=>{
                if(!config.manifest.categories.includes(cat)) throw new Error(`Category ${cat} unknown`);
            });
            return true;
        },
        "screenshots": (data) => {
            if (!Array.isArray(data)) throw new Error('Screenshots must be contained in Array');
            data.forEach(img=>{
                let url = new URL(img);
                if( config.manifest.supportedScreenshotFormats.indexOf( url.pathname.split('.').pop() ) === -1) throw new Error(`App Screenshots have to be one of types: ${config.manifest.supportedScreenshotFormats}`);
            });
            return true;
        },
        "defaults": (data)=>{
            if(!Array.isArray(data)) throw new Error('App Defaults have to be contained in array');
            return true;
        },
        "developer": (data)=>{
            if(typeof data !== 'object') throw new Error('App Developer data has to be contained in an object');
            if(!data.profile) throw new Error('App Developer profile url missing');
            try {
                let profileURL = new URL(data.profile);
                if(!profileURL) throw new Error();
            } catch(e) {
                //profileurl not valid
                throw new Error('Profile URL invalid');
            }
            if(!data.nick || typeof data.nick !== 'string') throw new Error('Developer nickname missing');
        },
        version: (data) => {
            if(!Array.isArray(data)) throw new Error('Version has to be array');
            if(data.length<3) throw new Error('version should contain 3parts');
            return true;
        }
    };
    // validate each key
    Object.entries(todo).forEach(([key, value])=>{
        if (!manifest[key]) throw new Error(`Your App Manifest is missing a ${key} property`);
        value(manifest[key]);
    });
    return true;
};

// verifiy app response after initialisation
const verifySource = ( code ) => {
    config.source.requiredFunctions.forEach(func=>{
        if(typeof code[func] !== 'function') throw new Error(`Missing function ${func}`);
    });
};

// prepare sourcecode for initialisation
const prepareSource = ( code ) => {
    // check if app attemts  registration
    if (code.indexOf('appdk.newApp')===-1) throw new Error('App not registered');

    // replace malicious code
    const replace_lookup_table = {
        ".parentNode": '',
        "appdk.newApp": 'return'
    };
    Object.entries(replace_lookup_table).forEach(([key, value])=>{
        code=code.replace(new RegExp(key,'g'), value);
    });
    return code;
};
const BuiltInOverwriteBlocks = ({
    appid='',
    appdk={}
}) => {
    let prefix = `${appid}.`;
    return {
        appdk:appdk,
        "document": {
            getElementById: (id) => {
                return document.getElementById(prefix+id);
            },
            getElementsByClassName: (classname) => {
                return document.getElementByClassName(prefix+classname);
            },
            getElementsByName: (name) => {
                return document.gteElementByClassName(prefix+name);
            },
            createElement: (json) => {
                return json2react(json, prefix);
            }
        },
        "window": {
            
        },
        "setTimeout": setTimeout,
        "setInterval": setInterval,
        "fetch": (url, conf) => {
            // check for network permission
        }
    };
};
// overwrite javascript builtins
const overwriteBuiltIn = (output='keys',{
    blocks = BuiltInOverwriteBlocks,
}) => {
    return Object[output](blocks);
};

// check manifest agains code
const manifestVSsource = ( manifest, source ) => {
    // pretent that source is manifest
    checkAppManifest(source);
    // check wheather manifest==source
    Object.entries(source).forEach(([key, value])=>{
        if(!manifest[key]) throw new Error(`Key ${key} not defined in manifest`);
        
        // compare rest
        if(!_.isEqual(value, manifest[key])) throw new Error(`Source ${key} doesn't match manifest`);
        return true;
    });
    return true;
};

const initialiseApp = ( manifest, code, appdk ) => {
    try {
        // setup app
        checkAppManifest(manifest);
        let sourceReady = prepareSource(code);
        let sourceFunc = new Function(...overwriteBuiltIn('keys',{
            blocks: BuiltInOverwriteBlocks({
                appid: manifest.id,
                appdk: appdk
            })
        }), sourceReady);
        let source = sourceFunc(...overwriteBuiltIn('values',{
            blocks: BuiltInOverwriteBlocks({
                appid: manifest.id,
                appdk: appdk
            })
        }));
        verifySource(source);
        manifestVSsource(manifest, source.packageInfo());
        // perform app tests
        let test_results = source.test(manifest.id);
        if(!test_results[0] || test_results[1]!==manifest.id) throw new Error('App Test failed');
        return {
            error: false,
            app: source
        };
    } catch (e) {
        return {
            error: true,
            msg: e
        };
    }
};

const addApp = (manifest, code) => new Promise((resolve, reject) => {
    
});
const fetchAppInfo = (repo, id, version='') => new Promise((resolve, reject)=>{
    try {
        let fl = pm.getState().vfs.readFileSync('/etc/tpm/mirrorlist');
        if(fl.error) throw new Error('Something is off with the mirrorlist');
        let cont = JSON.parse(fl.data.content);
        let url = cont[repo];
        if(!url) throw new Error('Repo not found');
        let repo_url = url;
        pm.getState().nm.fetch(`${url}/repo.json`)
            .then(d=>d.json())
            .then(json=>{
                let pkg = json.content.packages[id];
                if(!pkg) throw new Error("package not found");
                // found package
                let url = json.content.scheme;
                if(!!pkg.url) url = pkg.url;
                url=url.replace(new RegExp('{VERSION}','g'),(!version)?pkg.versions.pop():version);
                url=url.replace(new RegExp('{BASE_URL}','g'), json.content.baseUrl);
                url=url.replace(new RegExp('{REPO_URL}','g'), repo_url);
                url=url.replace(new RegExp('{PACKAGE_NAME}','g'), id);
                pm.getState().nm.fetch(`${url}.toss`)
                    .then(d=>d.blob())
                    .then(blob=>{
                        let zip = new JSZip();
                        zip.loadAsync(blob)
                            .then(unzipped=>{
                                (async ()=> {
                                    try {
                                    let js = await zip.file('app.tos.js').async('string');
                                        let manifest = await zip.file('app.tos.json').async('string');
                                    let cont = [];
                                    zip.forEach(d=>cont.push(d));
                                    resolve({
                                        content: cont,
                                        js: js,
                                        manifest: manifest
                                    });
                                    } catch(e) {
                                        reject(e);
                                    }
                                })();
                            });
                    })
                    .catch(e=>reject(e));
            });
    } catch (e) {
        reject(e);
    }
});
const updateRepo = (id) => new Promise((resolve, reject) =>{
    try {
        let result = {
            repoid: id,
            packages:{}
        };
        let fl = pm.getState().vfs.readFileSync('/etc/tpm/mirrorlist');
        if(fl.error) throw new Error('Something is off with the mirrorlist file');
        let cont = JSON.parse(fl.data.content);
        let url = cont[id];
        if(!url) throw new Error('Repo not found');
        pm.getState().nm.fetch(`${url}/repo.json`)
            .then(d=>d.json())
            .then(json=>{
                // repo data found
                result.info=json;
                // fetch all packages
                Object.keys(json.content.packages).forEach(async (pkg) => {
                    let req = await fetchAppInfo(id, pkg);
                    let inf = req;
                    result.packages[pkg]=inf;
                    if(Object.keys(result.packages).length===Object.keys(json.content.packages).length) {
                        //finished fethcing all packages
                        resolve(result);
                    }
                });
            })
            .catch(e=>{
                reject(e);
            });
    } catch(e) {
        reject(e);
    }
});
const updateAllRepos = () => new Promise((resolve, reject) => {
    let repos = {};
    try {
        let fl = pm.getState().vfs.readFileSync('/etc/tpm/mirrorlist');
        if(fl.error) throw new Error('Something is off with the mirrorlist file');
        let cont = JSON.parse(fl.data.content);
        Object.keys(cont).forEach(repoid=>{
            updateRepo(repoid)
                .then(json=>{
                    repos[repoid]=json;
                    if (Object.keys(repos).length===Object.keys(cont).length) {
                        // all repos fetched
                        resolve(repos);
                    }
                })
                .catch(e=>{
                    reject(e);
                });
        });
    } catch (e) {
        reject(e);
    }
});

const installApp = ({js, manifest, content}) => new Promise((resolve, reject)=>{
    try {
        let resp = initialiseApp(JSON.parse(manifest), js.trim());
        if(resp.error) throw new Error("Error initalising App: " + resp.msg);
        let fold = pm.getState().vfs.readFolderSync('/usr/apps');
        if(fold.error) throw new Error('Error reading app dir');
        // remove old version completly
        if(Object.keys(fold.data.content).includes(JSON.parse(manifest).id)) {
            if(pm.getState().vfs.deleteFolderSync(`/usr/apps/${JSON.parse(manifest).id}`).error) throw new Error('couldn\'t delete folder');
        }
        // create folder structure
        pm.getState().nm.fetch(JSON.parse(manifest).icon)
            .then(b=>b.blob())
            .then(blob=>{
                let reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = () => {
        let folder = pm.getState().vfs.makeFolderSync('/usr/apps',JSON.parse(manifest).id, {
            permission: FilePermission.DEFAULT,
            content: {
                files: new Folder({
                    permission: FilePermission.DEFAULT,
                    content:{}
                }),
                shared_preferences: new File({
                    permission: FilePermission.DEFAULT,
                    content: JSON.stringify({first_opened:true}),
                    bType: 'application/json'
                }),
                src: new Folder({
                    permission: FilePermission.DEFAULT,
                    content:{
                        manifest: new File({
                            permission: FilePermission.DEFAULT,
                            bType: 'application/json',
                            content: manifest
                        }),
                        main: new File({
                            permission: FilePermission.DEFAULT,
                            bType: 'application/javascript',
                            content: js.trim()
                        }),
                        icon: new File({
                            permission: FilePermission.DEFAULT,
                            bType:'image/png',
                            content:reader.result
                        })
                    }
                })
            }
        });
                    if(folder.error) throw new Error('Error creating app folder');
                    //installed
                    addAppToApps(
                        JSON.parse(manifest),
                        js.trim(),
                        reader.result
                    );
                    resolve(true);
                };
            })
            .catch(e=>{
                reject(e);
            });
    } catch(e) {
        reject(e);
    }
});
const listInstalledApps = () => {
    try {
        let folder = pm.getState().vfs.readFolderSync('/usr/apps');
        if(folder.error) throw new Error('Could not read app folder');
        let list = Object.entries(folder.data.content).map(([id, folder])=>{
            
            return {
                manifest:JSON.parse(folder.content.src.content.manifest.content),
                src: folder.content.src.content.main.content,
                id:id,
                icon: folder.content.src.content.icon.content
            };
        });
        return {
            error: false,
            msg:null,
            data: list
        };
    } catch(e) {
        return {
            error: true,
            msg: e,
            data: []
        };
    }
};

const addAppToApps = (manifest, src, icon) => {
    pm.getState().am.am.addApp(new PackageLayer(
        manifest,
        src,
        icon
    ));
};

export {
    addApp,
    updateAllRepos,
    checkAppManifest,
    config as TPMConfig,
    installApp,
    listInstalledApps,
    initialiseApp,
    addAppToApps
}
